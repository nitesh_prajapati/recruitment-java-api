package com.matchesfashion.papi.product.service;

import com.matchesfashion.papi.product.controller.model.ProductResponse;
import com.matchesfashion.papi.product.service.model.PageableList;
import com.matchesfashion.papi.product.service.model.Sort;

public interface ProductService {

	PageableList<ProductResponse> getProducts(int pageNumber, int pageSize, String sortField, Sort sort,
			Integer price);
}
