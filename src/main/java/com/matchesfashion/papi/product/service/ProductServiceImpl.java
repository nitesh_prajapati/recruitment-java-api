package com.matchesfashion.papi.product.service;

import static org.springframework.data.domain.Sort.Direction.fromString;
import static org.springframework.data.domain.Sort.by;

import java.util.stream.Collectors;

import com.matchesfashion.papi.product.controller.model.ProductResponse;
import com.matchesfashion.papi.product.domain.Product;
import com.matchesfashion.papi.product.repository.ProductRepository;
import com.matchesfashion.papi.product.service.model.PageableList;
import com.matchesfashion.papi.product.service.model.Sort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

	private final ProductRepository productRepository;

	public ProductServiceImpl(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public PageableList<ProductResponse> getProducts(int pageNumber, int pageSize, String sortField, Sort sort,
			Integer price) {
		final Page<Product> page;
		if (price == null) {
			page = productRepository
					.findAll(PageRequest.of(pageNumber - 1, pageSize, by(fromString(sort.name()), sortField)));
		} else {
			page = productRepository.findAllByPriceIsGreaterThan(price, PageRequest.of(pageNumber - 1, pageSize, by(fromString(sort.name()), sortField)));
		}
		return new PageableList(page.getNumber() + 1, page.getNumberOfElements(), page.getSize(), page.getTotalPages(),
				page.getTotalElements(), page.getContent().stream()
				.map(x -> new ProductResponse(x.getId(), x.getTitle(), x.getCategory(), x.getPrice()))
				.collect(Collectors.toList()));
	}
}
