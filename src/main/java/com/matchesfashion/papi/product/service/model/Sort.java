package com.matchesfashion.papi.product.service.model;

public enum Sort {
	ASC,
	DESC
}