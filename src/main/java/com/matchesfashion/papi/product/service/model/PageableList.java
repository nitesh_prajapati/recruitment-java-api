package com.matchesfashion.papi.product.service.model;

import java.util.List;

public class PageableList<T> {

	private int pageNumber;
	private int resultsSize;
	private int pageSize;
	private int totalPages;
	private long totalElements;
	private List<T> results;

	public PageableList() {
	}

	public PageableList(int pageNumber, int resultsSize, int pageSize, int totalPages, long totalElements, List<T> results) {
		this.pageNumber = pageNumber;
		this.resultsSize = resultsSize;
		this.pageSize = pageSize;
		this.totalPages = totalPages;
		this.totalElements = totalElements;
		this.results = results;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getResultsSize() {
		return resultsSize;
	}

	public void setResultsSize(int resultsSize) {
		this.resultsSize = resultsSize;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

	public List<T> getResults() {
		return results;
	}

	public void setResults(List<T> results) {
		this.results = results;
	}
}
