package com.matchesfashion.papi.product.controller;

import javax.validation.constraints.Min;

import com.matchesfashion.papi.product.controller.model.ProductResponse;
import com.matchesfashion.papi.product.service.ProductService;
import com.matchesfashion.papi.product.service.model.PageableList;
import com.matchesfashion.papi.product.service.model.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

	private ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	@GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PageableList<ProductResponse>> getProducts(
			@RequestParam(defaultValue = "1") @Min(1) int pageNumber,
			@RequestParam(defaultValue = "10") int pageSize,
			@RequestParam(defaultValue = "id") String sortField,
			@RequestParam(defaultValue = "DESC") Sort sort,
			@RequestParam(name = "price", required = false) Integer price) {
		final PageableList<ProductResponse> pageableList =
				productService.getProducts(pageNumber, pageSize, sortField, sort, price);
		return ResponseEntity.ok(pageableList);
	}
}
