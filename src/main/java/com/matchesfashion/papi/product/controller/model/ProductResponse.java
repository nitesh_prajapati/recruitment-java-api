package com.matchesfashion.papi.product.controller.model;

public class ProductResponse {

	private int id;

	private String title;

	private String category;

	private int price;

	public ProductResponse(int id, String title, String category, int price) {
		this.id = id;
		this.title = title;
		this.category = category;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
