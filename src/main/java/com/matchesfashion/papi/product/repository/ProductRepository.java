package com.matchesfashion.papi.product.repository;

import com.matchesfashion.papi.product.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	Page<Product> findAllByPriceIsGreaterThan(int price, Pageable pageable);
}
