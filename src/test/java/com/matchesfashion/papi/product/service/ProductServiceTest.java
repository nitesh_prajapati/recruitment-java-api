package com.matchesfashion.papi.product.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

import com.matchesfashion.papi.product.controller.model.ProductResponse;
import com.matchesfashion.papi.product.domain.Product;
import com.matchesfashion.papi.product.repository.ProductRepository;
import com.matchesfashion.papi.product.service.model.PageableList;
import com.matchesfashion.papi.product.service.model.Sort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

	@Mock
	private ProductRepository productRepository;

	@Mock
	private Product product1;

	@Mock
	private Product product2;

	@Mock
	private Page<Product> page;

	@InjectMocks
	private ProductServiceImpl productService;

	@Test
	public void getProducts_shouldReturn2Products() {
		// Arrange
		when(productRepository.findAll(any(Pageable.class))).thenReturn(page);
		when(page.getNumber()).thenReturn(0);
		when(page.getSize()).thenReturn(10);
		when(page.getNumberOfElements()).thenReturn(2);
		when(page.getTotalPages()).thenReturn(3);
		when(page.getTotalElements()).thenReturn(32L);
		when(page.getContent()).thenReturn(Arrays.asList(product1, product2));
		when(product1.getId()).thenReturn(1);
		when(product2.getId()).thenReturn(2);

		// Act
		final PageableList<ProductResponse> pageableList = productService.getProducts(1, 10, "id", Sort.DESC, null);

		// Assert
		assertThat(pageableList).isNotNull();
		assertThat(pageableList.getPageSize()).isEqualTo(10);
		assertThat(pageableList.getTotalPages()).isEqualTo(3);
		assertThat(pageableList.getPageNumber()).isEqualTo(1);
		assertThat(pageableList.getTotalElements()).isEqualTo(32);
		assertThat(pageableList.getResults().size()).isEqualTo(2);
		assertThat(pageableList.getResults().stream().map(ProductResponse::getId).collect(Collectors.toList()))
				.containsExactly(1, 2);
	}

	@Test
	public void getProducts_shouldReturnZeroProducts() {
		// Arrange
		when(productRepository.findAll(any(Pageable.class))).thenReturn(page);
		when(page.getNumber()).thenReturn(0);
		when(page.getSize()).thenReturn(10);
		when(page.getNumberOfElements()).thenReturn(0);
		when(page.getTotalPages()).thenReturn(1);
		when(page.getTotalElements()).thenReturn(0L);
		when(page.getContent()).thenReturn(Collections.emptyList());

		// Act
		final PageableList<ProductResponse> pageableList = productService.getProducts(1, 10, "id", Sort.DESC, null);

		// Assert
		assertThat(pageableList).isNotNull();
		assertThat(pageableList.getPageNumber()).isEqualTo(1);
		assertThat(pageableList.getPageSize()).isEqualTo(10);
		assertThat(pageableList.getTotalPages()).isEqualTo(1);
		assertThat(pageableList.getTotalElements()).isEqualTo(0);
		assertThat(pageableList.getResults().size()).isEqualTo(0);
	}
}
