package com.matchesfashion.papi.product.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.data.domain.Sort.by;

import java.util.Arrays;
import java.util.stream.Collectors;

import com.matchesfashion.papi.product.domain.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@DataJpaTest
@ExtendWith(SpringExtension.class)
class ProductRepositoryTest {

	@Autowired
	ProductRepository productRepository;

	@Test
	public void findAllByPriceIsGreaterThan_priceGreaterThan10_returns3Products() {
		final PageRequest pageRequest = PageRequest.of(0, 10, by(Sort.Direction.fromString("ASC"), "id"));
		final Page<Product> products = productRepository.findAllByPriceIsGreaterThan(10, pageRequest);
		assertThat(products).isNotNull();
		assertThat(products.getContent().size()).isEqualTo(3);
		assertThat(products.stream().map(x -> x.getPrice()).collect(Collectors.toList())
				.containsAll(Arrays.asList(100, 10000))).isTrue();
	}

	@Test
	public void findAllByPriceIsGreaterThan_priceGreaterThan100_returns1Product() {
		final PageRequest pageRequest = PageRequest.of(0, 10, by(Sort.Direction.fromString("ASC"), "id"));
		final Page<Product> products = productRepository.findAllByPriceIsGreaterThan(100, pageRequest);
		assertThat(products).isNotNull();
		assertThat(products.getContent().size()).isEqualTo(1);
		assertThat(
				products.stream().map(x -> x.getPrice()).collect(Collectors.toList()).containsAll(Arrays.asList(10000)))
				.isTrue();
	}

	@Test
	public void findAllByPriceIsGreaterThan_priceGreaterThan100000_returnsZeroProduct() {
		final PageRequest pageRequest = PageRequest.of(0, 10, by(Sort.Direction.fromString("ASC"), "id"));
		final Page<Product> products = productRepository.findAllByPriceIsGreaterThan(100000, pageRequest);
		assertThat(products).isNotNull();
		assertThat(products.getContent().size()).isEqualTo(0);
	}
}