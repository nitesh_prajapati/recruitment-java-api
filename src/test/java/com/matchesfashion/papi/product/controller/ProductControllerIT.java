package com.matchesfashion.papi.product.controller;

import static org.assertj.core.api.Assertions.assertThat;

import com.matchesfashion.papi.PapiApplication;
import com.matchesfashion.papi.product.service.model.PageableList;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest(classes = PapiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Sql("classpath:data.sql")
	@Test
	public void getAllProducts_defaultParam() {
		final ResponseEntity<PageableList> pageableListResponseEntity =
				testRestTemplate.getForEntity("http://localhost:" + port + "/products", PageableList.class);
		assertThat(pageableListResponseEntity).isNotNull();
		assertThat(pageableListResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		final PageableList pageableList = pageableListResponseEntity.getBody();
		assertThat(pageableList.getPageNumber()).isEqualTo(1);
		assertThat(pageableList.getPageSize()).isEqualTo(10);
		assertThat(pageableList.getResultsSize()).isEqualTo(3);
		assertThat(pageableList.getTotalPages()).isEqualTo(1);
		assertThat(pageableList.getTotalElements()).isEqualTo(3L);
		assertThat(pageableList.getResults().size()).isEqualTo(3);
	}

	@Test
	public void getAllProducts_withPageNumberAndSize() {
		final ResponseEntity<PageableList> pageableListResponseEntity = testRestTemplate
				.getForEntity("http://localhost:" + port + "/products?pageNumber=1&pageSize=1", PageableList.class);
		assertThat(pageableListResponseEntity).isNotNull();
		assertThat(pageableListResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		final PageableList pageableList = pageableListResponseEntity.getBody();
		assertThat(pageableList.getPageNumber()).isEqualTo(1);
		assertThat(pageableList.getPageSize()).isEqualTo(1);
		assertThat(pageableList.getResultsSize()).isEqualTo(1);
		assertThat(pageableList.getTotalPages()).isEqualTo(3);
		assertThat(pageableList.getTotalElements()).isEqualTo(3L);
		assertThat(pageableList.getResults().size()).isEqualTo(1);
	}

	@Test
	public void getProducts_costMoreThan10() {
		final ResponseEntity<PageableList> pageableListResponseEntity =
				testRestTemplate.getForEntity("http://localhost:" + port + "/products?price=10", PageableList.class);
		assertThat(pageableListResponseEntity).isNotNull();
		assertThat(pageableListResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		final PageableList pageableList = pageableListResponseEntity.getBody();
		assertThat(pageableList.getPageNumber()).isEqualTo(1);
		assertThat(pageableList.getPageSize()).isEqualTo(10);
		assertThat(pageableList.getResultsSize()).isEqualTo(3);
		assertThat(pageableList.getTotalPages()).isEqualTo(1);
		assertThat(pageableList.getTotalElements()).isEqualTo(3L);
		assertThat(pageableList.getResults().size()).isEqualTo(3);
		assertThat(pageableList.getResults().stream().anyMatch(x -> x.toString().contains("price=90"))).isTrue();
		assertThat(pageableList.getResults().stream().anyMatch(x -> x.toString().contains("price=100"))).isTrue();
	}

	@Test
	public void getProducts_costMoreThan100() {
		final ResponseEntity<PageableList> pageableListResponseEntity =
				testRestTemplate.getForEntity("http://localhost:" + port + "/products?price=100", PageableList.class);
		assertThat(pageableListResponseEntity).isNotNull();
		assertThat(pageableListResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		final PageableList pageableList = pageableListResponseEntity.getBody();
		assertThat(pageableList.getPageNumber()).isEqualTo(1);
		assertThat(pageableList.getPageSize()).isEqualTo(10);
		assertThat(pageableList.getResultsSize()).isEqualTo(1);
		assertThat(pageableList.getTotalPages()).isEqualTo(1);
		assertThat(pageableList.getTotalElements()).isEqualTo(1L);
		assertThat(pageableList.getResults().size()).isEqualTo(1);
		assertThat(pageableList.getResults().stream().anyMatch(x -> x.toString().contains("price=10000"))).isTrue();
	}

}

