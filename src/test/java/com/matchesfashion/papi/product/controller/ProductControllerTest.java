package com.matchesfashion.papi.product.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.matchesfashion.papi.product.controller.model.ProductResponse;
import com.matchesfashion.papi.product.service.ProductService;
import com.matchesfashion.papi.product.service.model.PageableList;
import com.matchesfashion.papi.product.service.model.Sort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {

	@Mock
	private ProductService productService;

	@Mock
	private ProductResponse productResponse1;

	@Mock
	private ProductResponse productResponse2;

	@Mock
	PageableList<ProductResponse> pageableList;

	@InjectMocks
	ProductController productController;

	@Test
	public void getProducts_shouldReturnZeroProducts() {
		when(productService.getProducts(1, 10, "id", Sort.DESC, null)).thenReturn(pageableList);
		when(pageableList.getResults()).thenReturn(Collections.emptyList());
		when(pageableList.getPageNumber()).thenReturn(1);
		when(pageableList.getPageSize()).thenReturn(10);
		when(pageableList.getTotalElements()).thenReturn(0L);
		when(pageableList.getTotalPages()).thenReturn(1);

		final ResponseEntity<PageableList<ProductResponse>> responseEntity =
				productController.getProducts(1, 10, "id", Sort.DESC, null);

		assertThat(responseEntity).isNotNull();
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(responseEntity.getBody().getResults().size()).isEqualTo(0);
		assertThat(responseEntity.getBody().getPageNumber()).isEqualTo(1);
		assertThat(responseEntity.getBody().getPageSize()).isEqualTo(10);
		assertThat(responseEntity.getBody().getTotalPages()).isEqualTo(1);
		assertThat(responseEntity.getBody().getTotalElements()).isEqualTo(0l);
	}

	@Test
	public void getProducts_shouldReturn2Products() {
		when(productService.getProducts(1, 10, "id", Sort.DESC, null)).thenReturn(pageableList);
		when(pageableList.getResults()).thenReturn(Arrays.asList(productResponse1, productResponse2));
		when(pageableList.getPageNumber()).thenReturn(1);
		when(pageableList.getPageSize()).thenReturn(10);
		when(pageableList.getTotalElements()).thenReturn(2l);
		when(pageableList.getTotalPages()).thenReturn(1);
		when(productResponse1.getId()).thenReturn(1);
		when(productResponse2.getId()).thenReturn(2);

		final ResponseEntity<PageableList<ProductResponse>> responseEntity =
				productController.getProducts(1, 10, "id", Sort.DESC, null);

		assertThat(responseEntity).isNotNull();
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		final PageableList<ProductResponse> pageableList = responseEntity.getBody();
		assertThat(pageableList).isNotNull();
		assertThat(pageableList.getResults().size()).isEqualTo(2);
		assertThat(pageableList.getPageNumber()).isEqualTo(1);
		assertThat(pageableList.getPageSize()).isEqualTo(10);
		assertThat(pageableList.getTotalPages()).isEqualTo(1);
		assertThat(pageableList.getTotalElements()).isEqualTo(2L);
		assertThat(pageableList.getResults().stream().map(ProductResponse::getId).collect(Collectors.toList()))
				.containsExactly(1, 2);
	}

	@Test
	public void getProducts_shouldReturn10Products() {
		when(productService.getProducts(2, 10, "id", Sort.DESC, null)).thenReturn(pageableList);
		when(pageableList.getResults()).thenReturn(getProductResponseList());
		when(pageableList.getPageNumber()).thenReturn(2);
		when(pageableList.getPageSize()).thenReturn(10);
		when(pageableList.getTotalElements()).thenReturn(32L);
		when(pageableList.getTotalPages()).thenReturn(4);

		final ResponseEntity<PageableList<ProductResponse>> responseEntity =
				productController.getProducts(2, 10, "id", Sort.DESC, null);

		assertThat(responseEntity).isNotNull();
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		final PageableList<ProductResponse> pageableList = responseEntity.getBody();
		assertThat(pageableList).isNotNull();
		assertThat(pageableList.getResults().size()).isEqualTo(10);
		assertThat(pageableList.getPageNumber()).isEqualTo(2);
		assertThat(pageableList.getPageSize()).isEqualTo(10);
		assertThat(pageableList.getTotalPages()).isEqualTo(4);
		assertThat(pageableList.getTotalElements()).isEqualTo(32l);
	}

	private List<ProductResponse> getProductResponseList() {
		List<ProductResponse> productResponses = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			productResponses.add(mock(ProductResponse.class));
		}
		return productResponses;
	}

}
